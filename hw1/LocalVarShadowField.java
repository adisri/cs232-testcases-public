/**
 *
 * Explanation:
 *  This test cases is trying to test if the implementation treats fields and
 *  local variable properly. In class A, we have a field call "field". Inside
 *  the A_callNOnly(), we use a local variable called "field" to shadow the field.
 *  "field" in callNOnly can only be initialized to new N(), but not new M();
 *  However, the class member "field" could have type M or N.
 *  So a proper implementation should NOT have a pair like
 *  A_callNOnly -> N_foo
 *  But if the implementation messed up, this could happen.
 */
class LocalVarShadowField {
    public static void main(String[] a){
        System.out.println(new A().callBoth());
        System.out.println(new A().callNOnly());
    }
}

class A {
    M field;

    public int callBoth(){
        int r1;
        int r2;
        int r3;
        field = new M();
        r1 = field.foo(10);
        r2 = this.assign1();
        r3 = field.foo(10);
        return 0;
    }

    public int callNOnly(){
        M field;
        int r1;
        field = new N();
        r1 = field.foo(10);
        return 0;
    }

    public int assign1(){
        field = new N();
        return 0;
    }
}

class M {
    public int foo(int a){
        return a;
    }
}

class N extends M{
    public int foo(int a){
        return a+1;
    }
}
