/*
	Test: Parent and children classes have the same function. 
	Call graph should generate the correct edges
*/

class Basic {
    public static void main(String[] a){
     	System.out.println(new Alpha().sum());
    }
}


class Alpha{
	A a1 ;
	B b1 ;
    B b2 ;
    B b3 ;

	public int sum(){
		int result ; 
		a1 = new A();
		b1 = new B();
    	b2 = new C();
    	b3 = new D();
    	result = this.add(this.add(a1.foo(), b1.foo()), b2.foo());
    	result = this.add(result, b3.foo());

		return result;
	}

	public int add(int a, int b){
        return a + b;
    }

    public int mult(int a, int b){
        return a * b;
    }
}

class A {

	public int foo(){
		return 0;
	}
}


class B extends A{

	public int foo(){
		return 1; 
	}
}


class C extends B{

	public int foo(){
		return 2; 
	}
}

class D extends B{

	public int foo(){
		return 3; 
	}
}
