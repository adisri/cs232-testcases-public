// this program theoretically prints out all positive numbers (42, every time), but if a negative number is fed in as input it will print that negative number.
// the numerical analyzer should be able to catch that a negative number will be printed.
class Main {
    public static void main(String[] a){
		System.out.println(new Num().setUp((0 - 11)));
        System.out.println(new Num().setUp(25));
		System.out.println(new Num().setUp(42));
    }
}

class Num {
	int n;
	
	public int setUp(int i) {
		n = i;
		if (n < 42) 
			n = this.addUp(n);
		else 
			n = this.subDown(n);
		return n;
	}
	
	public int addUp(int in) {
		if (in < 0) 
			System.out.println(in);
		else
			
		while (in < 42) {
			in = in + 1;
		}
		return in;
	}
	
	public int subDown(int in) {
		while (42 < in) {
			in = in - 1;
		}
		return in;
	}
}