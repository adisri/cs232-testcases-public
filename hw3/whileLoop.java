class Main {
    public static void main(String[] s){
      int iter;
      int initValue;
      A a;

      initValue = 5;
      a = new A();
      iter = a.init(initValue);
      while (!a.endLoop()){
        iter = a.decrement();
      } 

        System.out.println(iter); //iter should be -1 at this point
    }
}

class A {
  int check;

  public int init(int initValue){
    check = initValue;
    return check;
  }
  public boolean endLoop(){
    return (check < 0); 
  }

    public int decrement(){
      check = check - 1; 
      return check;
    }
}
