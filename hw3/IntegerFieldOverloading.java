/* 	Nifty aspect of input-output pair:
	This program includes inheritance, overloading, and an
	uninitialized integer field (default value should be 0).
	C.doNumberStuff returns a positive integer the first time
	its called. D.doNumberStuff returns a negative integer the
	first time its called. The return value is then printed.
*/

class IntegerFieldOverloading {
	public static void main(String[] args) {
		int i;
		C c;

		c = new D();
		i = c.doNumberStuff();
		System.out.println(i);
	}
}

class C {
	int i;

	public int doNumberStuff() {
		return i + 1;
	}
}

class D extends C {
	public int doNumberStuff() {
		int negative4;
		
		negative4 = 0;
		negative4 = negative4 - 4;
		
		while (negative4 < i) {
			i = i - 1;
		}
		return i;
	}
}

